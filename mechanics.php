<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- POPUPS -->
		<div class="popup-mask js-close"></div>

		<div class="popup-container">
			<div class="close js-close"></div>
			
			<!-- INSERT POPUP HTML HERE -->
			
			<div class="popup-wrap">
				<div class="popup popup-content" id="popdev-target">
					<!-- CUSTOM HTML FROM DEV HERE -->
				</div>

				<div class="popup popup-custom" id="custom">
					<h1>I AM A POPUP</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>

				<div class="popup popup-custom" id="custom02">
					<h1>I AM A POPUP CUSTOM02</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>
			</div>
		</div>
		
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">
		
			<div class="mechanics_wrapper">
				<h3>Promo Mechanics</h3>
				<h4>Who are qualified to Join</h4>
				<ol>
					<li>
						<p>
							All UnionPay International card holders ages 18 and above, and residents of the Philippines, are eligible to join.
						</p>
					</li>
					<li>
						<p>
							Employees of UnionPay International and those of its third-party partners are not qualified to join the promo.
						</p>
					</li>
				</ol>

				<h4>How to Join the Promo</h4>
				<ol>
					<li>
						<p>Qualified participants must first register their details at the promo website to send entries. Qualified participants must enter the following details in the registration page:
							<ol>
								<li><p>Full Name</p></li>
								<li><p>Address</p></li>
								<li><p>Contact Number</p></li>
								<li><p>Birthdate</p></li>
								<li><p>6-digit UnionPay Card Number</p></li>
								<li><p>Name of Issuing Bank</p></li>
								<li><p>Email Address</p></li>
								<li><p>Preferred Password for the promo site account</p></li>
							</ol>
						</p>
					</li>
					<li>
						<p>
							Upon approval of the registration, qualified participants may begin sending their entries. Every purchase worth five hundred pesos (Php500.00) made during the duration of the promo period, using UnionPay International card, earns qualified participants a valid raffle entry. If the purchase amount is not divisible by five hundred (Php500.00), the excess amount cannot be combined with other purchases to gain another entry. For example, a purchase of one thousand nine hundred pesos (Php1,900.00) will gain three (3) valid entries since it is composed of three (3) five hundred (500) and a remainder of four hundred (400). The excess four hundred (400) from the transaction cannot be topped up in the succeeding transaction to gain an additional entry.
						</p>
					</li>
					<li>
						<p>
							To send in the valid raffle entry, qualified participants must log-in to the promo website andt enter the following purchase details:
							<ol>
								<li><p>Transcation date (format:DD-MM-YYYY)</p></li>
								<li><p>Amount Spent (format:Phpxxx.xx)</p></li>
								<li><p class="red">Approval Code</p></li>
							</ol>
						</p>
					</li>
					<li>
						<p>
							Qualified participants may track the number of entries he or she sent in his promo website account.
						</p>
					</li>
					<li>
						<p>
							Duplicate entries, or entries bearing the same details as previous entries, will be disqualified and will not be included in the raffle
						</p>
					</li>
				</ol>

				<h4>How to Join the Promo</h4>
				<p>The following shall not qualify to win any prizes:</p>
				<ol>
					<li>
						<p>
							Entries sent by employees of UnionPay International and any of its third-party partners are deemed invalid and disqualified.
						</p>
					</li>
					<li>
						<p>
							Entries with purchase dates before and after the promo period are deemed invalid.
						</p>
					</li>
					<li>
						<p>
							Entries that did not meet the minimum spending requirement of five hundred pesos (Php500.00) are deemed invalid.
						</p>
					</li>
					<li>
						<p>
							Duplicate entries, or entries bearing the same details as previous entries, are deemed invalid. UnionPay International reserves the right to verify registration of participants and the validity of entries sent.
						</p>
					</li>
				</ol>
				<h4>Prizes</h4>
				<ol>
					<li>
						<p>
							There shall be three (3) winners of two (2) business class roundtrip tickets each to select Asian destinations per monthly raffle date courtesy of Philippine Airlines.
							<ol>
								<li><p>December Raffle: three (3) winners of two (2) business class roundtrip tickets each to Bangkok, Thailand</p></li>
								<li><p>January Raffle: three (3) winners of two (2) business class roundtrip tickets each to Hong Kong</p></li>
								<li><p>February Raffle: three (3) winners of (2) business class roundtrip tickets each to Seoul, South Korea</p></li>
							</ol>
						</p>
					</li>
					<li>
						<p>
							Accommodation, transportation to and from the airport, and other related expenses to using the prize shall be shouldered by the promo winners.
						</p>
					</li>
					<li>
						<p>
							Prizes are non-convertible to cash and non-transferable.
						</p>
					</li>
				</ol>
				<h4>Determination of Winners</h4>
				<ol>
					<li>
						<p>
							UnionPay International shall generate a list of entries based on the total number of entries received within the monthly raffle period. 
						</p>
					</li>
					<li>
						<p>
							A randomizer will be opened (www.random.org) and the first and last number of the entries will be inputted in the randomizer.
						</p>
					</li>
					<li>
						<p>
							Random numbers shall be generated to determine the winners.
						</p>
					</li>
					<li>
						<p>
							Representatives of UnionPay International and the Department of Trade and Industry shall be present during the generation of the list of entries and in the determination of winners.
						</p>
					</li>
				</ol>
				<h4>Announcement of Winners and Claiming of Prizes</h4>
				<ol>
					<li>
						<p>
							Winners will be notified via registered mail, email, and phone call within five (5) days after the raffle draw.
						</p>
					</li>
					<li>
						<p>
							The list of winners will be made available in the UnionPay International Promo Website and social media accounts.
						</p>
					</li>
					<li>
						<p>
							Winners will get instructions on how to claim their prizes from UnionPay International representatives through the same channels as how they were notified of their win.
						</p>
					</li>
					<li>
						<p>
							Unclaimed prizes after two (2) months since the announcement of winners shall be deemed forfeited in favor of UnionPay International.
						</p>
					</li>
				</ol>
				<h4>Control Measures</h4>
				<ol>
					<li>
						<p>
							UnionPay International shall keep a copy of information received via the promo website for a maximum duration of six (6) months after the promo period.
						</p>
					</li>
					<li>
						<p>
							The promo will be moderated by representatives of UnionPay International and its third-party partners.
						</p>
					</li>
					<li>
						<p>
							UnionPay International reserves the right to conduct a verification process for all potential winners before awarding any prize. Winners who fail the verification process will be disqualified. All decisions made by UnionPay International on all matters related to this promotion are final.
						</p>
					</li>
				</ol>
				<h4>Promo Period</h4>
				<ol>
					<li>
						<p>
							The promo will run from November 24, 2017 up to February 23, 2018.
						</p>
					</li>
					<li>
						<p>
							Monthly Raffle Draws will happen on:
							<ol>
								<li><p>December __, 2017 – cut-off period for sending entries will be on December 23.</p></li>
								<li><p>January __, 2018 – cut-off period for sending entries will be on January 23.</p></li>
								<li><p>February __, 2018 – cut-off period for sending entries will be on February 23.</p></li>
							</ol>
						</p>
					</li>
				</ol>
				<div class="mech_footer_details">
					<ul>
						<li>Per DTI-FTEB Permit No.</li>
						<li>Supervised by the Bangko Sentral ng Pilipinas</li>
						<li>Telephone number: (632) 708-7087</li>
						<li>Email address: consumeraffairs@bsp.gov.ph</li>
					</ul>
				</div>

				<a href="javascript:void(0)"class="btn_teal prog_buttons" data-prog="next">Okay</a>
			</div>

			

			<!-- content -->
			<!-- <a href="javascript:void(0)" onclick="popOpen('custom')">Trigger popup</a> -->
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>







