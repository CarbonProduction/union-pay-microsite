<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- POPUPS -->
		<div class="popup-mask js-close"></div>
		
		<div class="popup-container">
			<div class="close js-close"></div>
			
			
			<div class="popup-wrap">
				<div class="popup popup-content" id="popdev-target">
					<!-- CUSTOM HTML FROM DEV HERE -->
				</div>
		
				<div class="popup popup-custom" id="forgot_pass">
					<h1>I AM A POPUP</h1>
		
					<p>Forgot your password? No problem. Enter your email here and we'll send you a link to reset it.</p>

					<form action="">
						<input type="text" placeholder="Email Address">
					</form>
		
					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
			
				</div>
		
				<div class="popup popup-custom" id="custom02">
					<h1>I AM A POPUP CUSTOM02</h1>
		
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>
		
					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
			
				</div>
			</div>
		</div>
		
		
		<!-- HEADER -->
		<header>
			<?php /*include('includes/header.php');*/ ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">
			<div class="maincontent">
				<div class="copy">
					<h1>Travel Your Way</h1>
					<p>Win round-trip business class tickets with UnionPay!</p>
				</div>

				<div class="form_wrapper">
					<form action="" class="login_form">
						<input type="text" class="login_input" placeholder="Email Address">
						<input type="text" class="login_input" type="Password" placeholder="Password">
						<button class="btn_red">Login</button>
						<p><a href="forgot_pass.php">Forgot Password</a>?</p>
						<p>Don't have an account? <a href="register.php">Register</a></p>
					</form>
				</div>
				
				<div class="nav_footer">
					<a href="mechanics.php">Mechanics</a>
					<a href="faq.php">FAQ</a>
					<a href="contact.php">Contact Us</a>
				</div>
			</div>
			

			<!-- content -->
			<!-- <a href="javascript:void(0)" onclick="popOpen('custom')">Trigger popup</a> -->
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>

