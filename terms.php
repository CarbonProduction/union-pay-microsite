<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- POPUPS -->
		<div class="popup-mask js-close"></div>

		<div class="popup-container">
			<div class="close js-close"></div>
			
			<!-- INSERT POPUP HTML HERE -->
			
			<div class="popup-wrap">
				<div class="popup popup-content" id="popdev-target">
					<!-- CUSTOM HTML FROM DEV HERE -->
				</div>

				<div class="popup popup-custom" id="custom">
					<h1>I AM A POPUP</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>

				<div class="popup popup-custom" id="custom02">
					<h1>I AM A POPUP CUSTOM02</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>
			</div>
		</div>
		
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">
		
			<div class="terms_wrapper reg_prog active">
				<h3>UNIONPAY <br/>INTERNATIONAL PROMO WEBSITE USAGE TERMS</h3>
				<p>This section contains the Terms of Use of *insert URL here* (hereinafter referred to as “This Website”) owned and operated by UnionPay International Co., Ltd. (hereinafter referred to as “UnionPay International”). By accessing This Website, and any of its pages and attachments, Users (hereinafter referred to as “You”) agree to abide by the UnionPay International Promo Website Usage Terms (hereinafter referred to as “Usage Terms”).</p>
				<p>The Usage Terms are in addition to those that apply to any accounts or credit card products You may have with UnionPay International and/or with its partner issuing banks. Where appropriate, please review these Usage Terms.</p>

				<h4>Use of Information and Materials</h4>
				<p>This Website is a facility hosted by UnionPay International for the purpose of allowing You to access information and send entries with regards to the UnionPay International promotion.</p>
				<p>Information contained in This Website are believed to be reliable but UnionPay International does not warrant its completeness, timeliness, or accuracy. Information on This Website and its pages are subject to change without notice.</p>
				<p>The information contained in these pages are not intended to provide professional or financial advice. You should exercise reasonable effort to obtain professional advice where deemed necessary.</p>

				<h4>Copyright and Trademarks</h4>
				<p>All logos, trademarks, service marks, and images on the pages of This Website are owned by UnionPay International and should not be replicated, reproduced, or otherwise used commercially or for public use without the written consent of UnionPay International.</p>

				<h4>Disclaimer of Warranties</h4>
				<p>UnionPay International does not warrant the accuracy, adequacy, timeliness, or the completeness of the materials and information contained herein. Information presented in these pages are “as is” or “as available” without any warranty of any kind, either expressed or implied. Specifically, no warranty relating to non-infringement, security, accuracy, fitness for purpose of freedom from computer viruses is given in connection with such information and materials.</p>
				<p>In no event shall UnionPay International be held liable for any special, incidental, indirect, or consequential damages of any kind, or any damages whatsoever resulting from loss of data, or profits, or whether or not advised of the possibility of damage, and on any theory of liability, arising out or in connection with the use or performance of information contained in This Website</p>

				<h4>Linked Websites</h4>
				<p>UnionPay International is not responsible for content and material in other websites linked to This Website. Your decision to enter said websites are at your own risk and shall be subject to the terms and conditions indicated on said linked websites. The provision of hyperlinks to other websites is not an indication of UnionPay International’s endorsement, approval, or guarantee of any third party.</p>

				<h4>Changes</h4>
				<p>UnionPay International reserves the right to administer changes to This Website and any of its pages without giving prior notice.</p>

				<h4>Submissions</h4>
				<p>All information submitted to UnionPay International via This Website shall be deemed and remain the property of UnionPay International. UnionPay International shall be free to use, for any purpose, any idea, concepts, or techniques gathered in This Website. UnionPay International shall not be subject to any obligations of confidentiality regarding submitted information except as required by law. Nothing contained herein shall be construed as limiting or reducing UnionPay International’s responsibilities and obligations to customers in accordance with UnionPay International’s Privacy Policy to consumers.</p>

				<h4>Limitation of Liability</h4>
				<p>In no event will UnionPay International be liable for any damages, including without limitation, direct or indirect, special, incidental or consequential damages, losses or expenses arising in connection with This Website or any linked site or use thereof, or inability to use by any party or in connection with any failure of performance, error omission, interruption, defect, delay in operation or transmission, computer virus or line or system failure, even if UnionPay International or its representatives thereof, are advised of the possibility of such damages, losses, or expenses.</p>

				<button class="btn_teal prog_buttons" data-prog="next">Okay</button>
			</div>

			

			<!-- content -->
			<!-- <a href="javascript:void(0)" onclick="popOpen('custom')">Trigger popup</a> -->
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>







