<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- POPUPS -->
		<div class="popup-mask js-close"></div>

		<div class="popup-container">
			<div class="close js-close"></div>
			
			<!-- INSERT POPUP HTML HERE -->
			
			<div class="popup-wrap">
				<div class="popup popup-content" id="popdev-target">
					<!-- CUSTOM HTML FROM DEV HERE -->
				</div>

				<div class="popup popup-custom" id="custom">
					<h1>I AM A POPUP</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>

				<div class="popup popup-custom" id="custom02">
					<h1>I AM A POPUP CUSTOM02</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>
			</div>
		</div>
		
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">
		
			<div class="mechanics_wrapper">
				<h3>UnionPay FAQs </br>for Usage Promo in Philippines</h3>
				<h4>24 Nov 2017 to 28 Feb 2018</h4>
				<ul>
					<li>
						<h4>How can I apply for a UnionPay card in The Philippines </h4>
						<p>Thank you for your interest to apply for a UnionPay Card. In Philippines, BDO, BOC, Omnipay, PNB and RCBC issue a range of UnionPay Credit, Debit and Prepaid Cards offering various privileges. Please visit www.unionpayintl.com or the official website of our partner banks for more information to help you select a card that best suits your needs.</p>
					</li>
					<li>
						<h4>Where can I use my UnionPay Card in Philippines?</h4>
						<p>UnionPay Cards are accepted at major supermarkets, department stores, retail, lifestyle and F&B outlets across Philippines. UnionPay Cards are also accepted in 162 countries and regions globally. Visit www.unionpayintl.com for ongoing UnionPay promotions in Philippines and at your favourite destinations.</p>
					</li>
					<li>
						<h4>How could I know all the promotions available in Philippines for UnionPay Cardholders?</h4>
						<p>There is a wide range of exciting offers for UnionPay Cardholders in Philippines and abroad. Do keep a lookout on our website at www.unionpayintl.com regularly for updates. </p>
					</li>
					<li>
						<h4>I am a shop owner and I wish to install POS machine to accept payment from customers. How can I do so?</h4>
						<p>Thank you for your interest to work with UnionPay. For more information, please contact your acquiring bank. Alternatively, you may provide us your name, e-mail address and contact number via “contact us” and we will be glad to follow up with you shortly. Have a great day ahead! </p>
					</li>
					<li>
						<h4>I own an e-commerce website, how can I apply for UnionPay payment gateway for my site?</h4>
						<p>Thank you for your support and interest to work with UnionPay. Please provide us your name, e-mail address and contact number via “contact us” and we will be glad to follow up with you shortly. Have a great day ahead!</p>
					</li>
					<li>
						<h4>How can I check the exchange rate on a particular date where my purchase was made? </h4>
						<p>The exchange rate is set by the bank that issues your UnionPay Card. We would like to suggest checking with the bank directly as they will be better equipped to assist you on this matter. Have a great day ahead! </p>
					</li>
					<li>
						<h4>I got rejected on the application for UnionPay Card, could you help?</h4>
						<p>We are sorry to hear that your application for a UnionPay Card was not approved. Application outcomes for UnionPay Cards are decided solely by the respective banks. We would like to suggest contacting the bank directly for more details. Have a great day ahead!  </p>
					</li>
					<li>
						<h4>Is there any extra charges when making a payment outside Philippines?</h4>
						<p>Charges related to cross-border transactions are set by the respective banks. We would like to suggest contacting your bank directly for more details. Have a great day ahead!  </p>
					</li>
					<li>
						<h4>Why can't I enjoy the promotion at a particular merchant when the T&Cs are right?</h4>
						<p>We are sorry for any inconvenience caused. Meanwhile, please provide your name and contact number via “contact us” and we’ll follow up with you shortly. Have a great day ahead!  </p>
					</li>
					<li>
						<h4>Merchant rejects payment by UnionPay card, what should I do? </h4>
						<p>We are sorry for any inconvenience caused. Please provide us the name of the merchant, location of the outlet, your name and contact number via “contact us” with a short description of the incident. We will follow up with you shortly. </p>
					</li>
					<li>
						<h4>I lost my card, what should I do?</h4>
						<p>Please inform the issuing bank as soon as possible to report the issue. </p>
					</li>
					<li>
						<h4>Can I use my UnionPay Card for Uber/ Grab Apps</h4>
						<p>UnionPay Cards issued in Philippines can be used for payment on both Grab and Uber. </p>
					</li>
				</ul>

				<a href="javascript:void(0)"class="btn_teal prog_buttons" data-prog="next">Okay</a>
			</div>

			

			<!-- content -->
			<!-- <a href="javascript:void(0)" onclick="popOpen('custom')">Trigger popup</a> -->
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>







