<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- POPUPS -->
		<div class="popup-mask js-close"></div>

		<div class="popup-container">
			<div class="close js-close"></div>
			
			<!-- INSERT POPUP HTML HERE -->
			
			<div class="popup-wrap">
				<div class="popup popup-content" id="popdev-target">
					<!-- CUSTOM HTML FROM DEV HERE -->
				</div>

				<div class="popup popup-custom" id="custom">
					<h1>I AM A POPUP</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>

				<div class="popup popup-custom" id="custom02">
					<h1>I AM A POPUP CUSTOM02</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>
			</div>
		</div>
		
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">
			<h1>Hi Jasper!</h1>

			<div class="entries_info_nav">
				<div class="entries_sel" data-view="approved">
					<h3 class="entry_count">8</h3>
					<p>Total Entries</p>
				</div>
				<div class="entries_sel" data-view="pending">
					<h3 class="pending_entry_count">2</h3>
					<p>Pending Codes</p>
				</div>
			</div>

			<div class="total_entries">
				<table>
					<tr>
						<th>Entry Date</th>
						<th>Approval No.</th>
						<th class="numofent">No. of Entries</th>
					</tr>
					<tr>
						<td>10-04-17</td>
						<td>x10a204065bxu</td>
						<td class="numofent">8</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</div>

			
			<!-- <button class="btn_red">Log-out</button> -->

			<!-- content -->
			<!-- <a href="javascript:void(0)" onclick="popOpen('custom')">Trigger popup</a> -->
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>

