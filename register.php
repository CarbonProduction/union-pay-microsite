<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- POPUPS -->
		<div class="popup-mask js-close"></div>

		<div class="popup-container">
			<div class="close js-close"></div>
			
			<!-- INSERT POPUP HTML HERE -->
			
			<div class="popup-wrap">
				<div class="popup popup-content" id="popdev-target">
					<!-- CUSTOM HTML FROM DEV HERE -->
				</div>

				<div class="popup popup-custom" id="reg_confirm">
					<h2>Registration Successful</h2>

					<p>You have successfully registered your account. You may now login your account.</p>

					<a href="javascript:void(0)" class="btn_teal js-close">Okay</a>
	
				</div>

				<!-- <div class="popup popup-custom" id="custom02">
					<h1>I AM A POPUP CUSTOM02</h1>
				
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>
				
					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
				
				</div> -->
			</div>
		</div>
		
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">
		
			<div class="register_account">
				<div class="reg_form_wrapper">
					<h3>Create Account</h3>
					<form action="javascript:void(0);" class="register_form">
						<div class="input_container">
							<input type="hidden" name="_token" value="">
							<div class="field_wrap">
								<input type="text" class="register_input" placeholder="Email">
								<span><strong>Email field is Required</strong></span>
							</div>
							<div class="field_wrap">
								<input type="Password" class="register_input" placeholder="Create Password">
								<span><strong>Password field is Required</strong></span>
							</div>
							<div class="field_wrap">
								<input type="password" class="register_input" placeholder="Confirm Password">
								<span><strong>Confirm Password field is Required</strong></span>
							</div>

							<div class="field_wrap">
								<input type="text" class="register_input" placeholder="Name">
								<span><strong>Name field is Required</strong></span>
							</div>
							<div class="field_wrap">
								<input type="text" class="register_input" placeholder="Address">
								<span><strong>Address field is Required</strong></span>
							</div>
							<div class="field_wrap">
								<input type="text" class="register_input" placeholder="Contact Number">
								<span><strong>Contact Number field is Required</strong></span>
							</div>

							<div class="field_wrap">
								<input type="text" class="register_input" placeholder="Issuing Bank">
								<span><strong>Name field is Required</strong></span>
							</div>
							<div class="field_wrap reg_cardnum">
								<label for="first_six">62</label>
								<input id="first_six" onkeyup="lengthCheck(this, 6)" type="text" class="register_input" maxlength="4" placeholder="+ First 4 Digits of Card">
								<span><strong>error</strong></span>
							</div>
							<div class="field_wrap">
								<input onkeyup="lengthCheck(this, 4)" onkeyup="" type="text" class="register_input" maxlength="4" placeholder="Last 4 Digits of Card">
								<span><strong>error</strong></span>
							</div>
							
						</div>
						<div class="term_check">
							<input type="checkbox" id="accept_term">
							<p>I accept the <a href="terms.php">Terms & Conditions</a></p>
						</div>
						<button class="btn_teal inactive" onclick="popOpen('reg_confirm')">Create New Profile</button>
					</form>
				</div>
			</div>

			

			<!-- content -->
			<!-- <a href="javascript:void(0)" onclick="popOpen('custom')">Trigger popup</a> -->
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>

