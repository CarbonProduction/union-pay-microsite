@extends('layouts.app')

@section('content')
<div class="mechanics_wrapper">
	<h3>UNIONPAY INTERNATIONAL (PHILIPPINES) </br>“SPEND & WIN BUSINESS CLASS TICKETS” PROMOTION</h3>
	<h3>24 NOV 2017 – 28 FEB 2018</h3>
	<h4>PARTICIPATION CRITERIA</h4>
	<ol>
		<li>
			<p>
				The “Spend & Win Business Class Tickets” Usage Campaign (the “Promo”) is open to Filipino citizens and all foreigners having resident status in the Philippines, except for employees of UnionPay International Co. Ltd, their advertising agencies and their relatives up to second degree of consanguinity or affinity.
			</p>
		</li>
		<li>
			<p>
				Participants must have reached the age of eighteen (18) years old as at 2017.
			</p>
		</li>
		<li>
			<p>
				Participants must use UnionPay Cards starting with “62” issued in Philippines by BDO, BOC, Omnipay, PNB and/OR RCBC Bankard.   
			</p>
		</li>
	</ol>
	
	<h4>PROMO PERIOD</h4>
	<ol>
		<li>
			<p>
				The promo is from November 24, 2017 (12:00mn) up to February 28, 2018 (11:59pm) (both dates inclusive).
			</p>
		</li>
		<li>
			<p>
				Monthly Raffle Draws will happen on:
			</p>
			<table>
				<tr>
					<th>
						<h4>DRAW DATE</h4>
					</th>
					<th>
						<h4>TRANSACTION DATE</h4>
					</th>
					<th>
						<h4>CUT-OFF FOR SENDING ENTRIES</h4>
					</th>
				</tr>
				<tr>
					<td>January 12, 2018 (5PM)</td>
					<td>February 12, 2018 (5PM)</td>
					<td>March 12, 2018 (5PM)</td>
				</tr>
				<tr>
					<td>Nov.24, 2017 (12MN) – Dec.31, 2017 (11:59PM)</td>
					<td>Nov.24, 2017 (12MN) – Jan.31, 2018 (11:59PM)</td>
					<td>Nov.24, 2017 (12MN) – Feb.28, 2018 (11:59PM)</td>
				</tr>
				<tr>
					<td>Dec.31, 2017 (11:59PM)</td>
					<td>Jan.31, 2018 (11:59PM)</td>
					<td>Feb.28, 2018 (11:59PM)</td>
				</tr>
			</table>
		</li>
	</ol>

	<h4>HOW TO JOIN THE PROMO</h4>
	<ol>
		<li>
			<p>1.	Participants must first register their details at the promo website at <a href="www.unionpayspendandwin.com.ph">www.unionpayspendandwin.com.ph</a> with the following details:
				<ol>
					<li><p>Full name (as per identity card)</p></li>
					<li><p>Date of Birth</p></li>
					<li><p>Residential address</p></li>
					<li><p>Contact number</p></li>
					<li><p>Email address (which will act as the user ID for the website)</p></li>
					<li><p>Preferred password for the promo site account</p></li>
					<li><p>Name of Card issuing bank(s) and corresponding Card numbers (first 6 and last 4 digits)</p></li>
				</ol>
			</p>
		</li>
		<li>
			<p>
				Upon successful registration, qualified participants may begin sending their entries. Every five hundred pesos (Php500.00) spend during the promo period, using UnionPay Card starting with “62” issued in Philippines, earns qualified participants ONE valid raffle entry.
			</p>
		</li>
		<li>
			<p>
				If the purchase amount is not divisible by five hundred (Php500.00), the excess amount cannot be combined with other purchases to gain another entry. For example, a purchase of one thousand nine hundred pesos (Php1,900.00) will gain three (3) valid entries since it is composed of three (3) five hundred (500) and a remainder of four hundred (400). The excess four hundred (400) from the transaction cannot be topped up in the succeeding transaction to gain an additional entry.
			</p>
		</li>
		<li>
			<p>
				To send in the valid raffle entry, qualified participants must log-in to the promo website and enter the following purchase details:
				<ol>
					<li><p>Approval Code</p></li>
					<li><p>Amount Spent (format: PhpXXX.XX)</p></li>
					<li><p>Select corresponding registered UnionPay Card</p></li>
				</ol>
			</p>
		</li>
		<li>
			<p>
				Qualified participants may track the number of entries and chances in the promo website.
			</p>
		</li>
	</ol>

	<h4>INVALID ENTRIES / DISQUALIFICATION</h4>
	<p>The following shall not qualify to win any prizes:</p>
	<ol>
		<li>
			<p>
				Entries sent by employees of UnionPay International Co. Ltd, its agencies, promo partners, and their relatives up to the second degree of consanguinity or affinity are deemed invalid and disqualified.
			</p>
		</li>
		<li>
			<p>
				Entries with purchase dates before and after the promo period are deemed invalid.
			</p>
		</li>
		<li>
			<p>
				Entries that did not meet the minimum spending requirement of five hundred pesos (Php500.00) are deemed invalid.
			</p>
		</li>
		<li>
			<p>
				Duplicate entries, or entries bearing the same details as previous entries, are deemed invalid.
			</p>
		</li>
		<li>
			<p>
				UnionPay International shall disqualify any participant or entry that does not comply with the criteria stated in these Terms and Conditions and withhold prizes from any participant who has breached any of these Terms and Conditions.
			</p>
		</li>
	</ol>
	<h4>PRIZES</h4>
	<ol>
		<li>
			<p>
				There will be three (8) winners of Philippine Airlines Travel Certificates for business class roundtrip tickets for two (2):
				<ol>
					<li><p>January Raffle: one (1) winner of two (2) travel certificates for business class roundtrip tickets each to Hong Kong (MNL-HKG-MNL) and one (1) winner of two (2) travel certificates for business class roundtrip tickets each to Taipei, Taiwan (MNL-TPE-MNL)</p></li>
					<li><p>February Raffle: one (1) winner of two (2) travel certificates business for class roundtrip tickets each to Bangkok, Thailand (MNL-BKK-MNL), one (1) winner of two (2) travel certificates for business class roundtrip tickets each to Hong Kong (MNL-HKG-MNL) and one (1) winner of two (2) travel certificates for business class roundtrip tickets each to Taipei, Taiwan (MNL-TPE-MNL)</p></li>
					<li><p>March Raffle: one (1) winner of two (2) travel certificates for business class roundtrip tickets each to Hong Kong (MNL-HKG-MNL), one (1) winner of two (2) travel certificates for business class roundtrip tickets each to Taipei, Taiwan (MNL-TPE-MNL), and one (1) winner of (2) travel certificates for business class roundtrip tickets each to Seoul, South Korea (MNL-ICN-MNL)</p></li>
				</ol>
			</p>
		</li>
		<li>
			<p>
				Accommodation, transportation to and fro the airport, and other related expenses to using the prize shall be shouldered by the promo winners.
			</p>
		</li>
		<li>
			<p>
				Travel Certificate is valid within 6 months from receipt. Booking must be at least 14 days prior to departure date with minimum stay of 3 days up to a maximum stay of 14 days. For travel to Seoul, South Korea (MNL-ICN-MNL), travel may only be done during the following dates: March 1 to 14, 2018; May 1 to July 26, 2018; or August 16 to December 16, 2018.
			</p>
		</li>
		<li>
			<p>Prizes are exchangeable for cash and are transferable. Cash conversion value of the prizes are as follows using P52.90 to USD exchange rate:
				<ul>
					<li>1 Business class roundtrip tickets each to Bangkok, Thailand: <b>P38,373.66 each. Total of P76,747.32</b></li>
					<li>3 Business class roundtrip tickets each to Hong Kong: <b>P29,825.02 each. Total of P178,950.12</b></li>
					<li>3 Business class roundtrip tickets each to Taipei, Taiwan: <b>P26,587.54 each. Total of P159,525.24</b></li>
					<li>1 Business class roundtrip tickets each to Seoul, South Korea: <b>P51,782.81 each. Total of P103,567.62</b></li>
				</ul>
			</p>
			
		</li>
	</ol>
	<h4>DETERMINATION OF WINNERS</h4>
	<ol>
		<li>
			<p>
				UnionPay International and/or its agencies shall generate a list of entries based on the total number of entries received within the monthly raffle period.
			</p>
		</li>
		<li>
			<p>
				A randomizer will be opened <a href="www.random.org">www.random.org</a> and the first and last number of the entries will be inputted in the randomizer.
			</p>
		</li>
		<li>
			<p>
				Random numbers shall be generated to determine the winners.
			</p>
		</li>
		<li>
			<p>
				Representatives of UnionPay International and the Department of Trade and Industry shall be present during the generation of the list of entries and in the determination of winners.
			</p>
		</li>
		<li>
			<p>Non-winning entries shall be included in the succeeding draws.</p>
		</li>
	</ol>
	<h4>ANNOUNCEMENT OF WINNERS AND CLAIMING OF PRIZES</h4>
	<ol>
		<li>
			<p>
				Winners will be notified via registered mail, email, and phone call within five (5) days after the raffle draw.
			</p>
		</li>
		<li>
			<p>
				The list of winners will be made available in the UnionPay International Promo Website and/or social media accounts.
			</p>
		</li>
		<li>
			<p>
				Winners will get instructions on how to claim their prizes from UnionPay International representatives and/or its agencies through the same channels as how they were notified of their win.
			</p>
		</li>
		<li>
			<p>
				The winner has 60 days to claim the prize (travel certificate) upon receipt of registered notice and to provide to the UnionPay International and/or its agencies with his/her contact and identification details, failing which, the prize may be forfeited, upon prior approval of DTI 
			</p>
		</li>
		<li>
			<p>
				The winner can only win once (1) during the promo period. In the event that a winner’s name is drawn in a succeeding draw, the winner must choose which prize he or she wants to claim. The other prize shall then be included for raffle in the next draw. 
			</p>
		</li>
		<li>
			<p>
				UnionPay International and/or its agencies reserve the right to use the winners’ names, images and comments relating to their contest experience for the purpose of any announcement or promotional, marketing or publicity purposes in any media without any fee being paid. 
			</p>
		</li>
	</ol>
	<h4>CONTROL MEASURES</h4>
	<ol>
		<li>
			<p>
				DTI representative will be invited to check the promo system before the approval of the promo DTI representative will invited to witness the drawing of winners.
			</p>
		</li>
		<li>
			<p>
				UnionPay International shall keep a copy of information received via the promo website for a maximum duration of six (6) months after the promo period. 
			</p>
		</li>
		<li>
			<p>
				The promo will be moderated by representatives of UnionPay International: Julie Monzon – Marketing Manager, UPI and its agencies: Chinkey Llave – Business Director, Dentsu x
			</p>
		</li>
		<li>
			<p>
				UnionPay International reserves the right to conduct a verification process for all potential winners before awarding any prize. Winners who fail the verification process will be disqualified. All decisions made by UnionPay International on all matters related to this promotion, in concurrence with DTI, are final.
			</p>
		</li>
	</ol>
	<h4>OTHERS</h4>
	<ol>
		<li>
			<p>
				The organizer reserves the right to request the winner to provide proof of identity and age and/or eligibility (if required) in order to claim a prize. Winner should present a valid government issued ID with picture, birthday, age, address and signature. In the event that a winner cannot provide suitable proof, the winner will forfeit the prize in whole and no substitute will be offered. Only the following I.D.s and documents shall be accepted as proof of identity:
				<ul>
					<li>Passport ID</li>
					<li>Unified Multi-Purpose ID</li>
					<li>NBI Clearance (issued within 6 months prior to the start of the promo period)</li>
					<li>Voter’s ID</li>
					<li>Driver’s License</li>
					<li>PRC ID</li>
					<li>Postal ID (new)</li>
				</ul>
			</p>
		</li>
		<li>
			<p>
				The participant shall assume all risks in respect of loss, injury, damage or liability which may arise as a result of or in connection to the participant’s participation in the contest, and shall not hold the organizer responsible in respect thereof except for liability which cannot be excluded by law.
			</p>
		</li>
		<li>
			<p>
				Participants understand and agree that in order to take part and win the Raffle, the organizer must collect and use personally identifiable information about Participants and will from time to time contact Participants via their e-mail and/or other addresses as provided. 
			</p>
		</li>
	</ol>
	<div class="mech_footer_details">
		<ul>
			<li>Per DTI-FTEB Permit No.</li>
			<li>Supervised by the Bangko Sentral ng Pilipinas</li>
			<li>Telephone number: (632) 708-7087</li>
			<li>Email address: consumeraffairs@bsp.gov.ph</li>
		</ul>
	</div>

	<a href="{{url('/')}}" class="btn_teal prog_buttons" data-prog="next">Okay</a>
</div>
@endsection