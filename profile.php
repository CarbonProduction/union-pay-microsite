<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- POPUPS -->
		<div class="popup-mask js-close"></div>

		<div class="popup-container">
			<div class="close js-close"></div>
			
			<!-- INSERT POPUP HTML HERE -->
			
			<div class="popup-wrap">
				<div class="popup popup-content" id="popdev-target">
					<!-- CUSTOM HTML FROM DEV HERE -->
				</div>

				<div class="popup popup-custom" id="entry_confirm">
					<h2>Congratulations Jasper!</h2>

					<p>You have successfully add an entry. Send more entries for more chances of winning a Japan trip or an iPhone X!.</p>

					<a href="javascript:void(0)" class="btn_teal js-close">Okay</a>
	
				</div>

				<div class="popup popup-custom" id="card_confirm">
					<h2>Congratulations Jasper!</h2>

					<p>You have successfully added new card.</p>

					<a href="javascript:void(0)" class="btn_teal js-close">Okay</a>
	
				</div>
			</div>
		</div>
		
		
		<!-- HEADER -->
		<header>
			<?php /*include('includes/header.php');*/ ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">
			<div class="profile_head">
				<h1>Hi Jasper!</h1>			
				<h5>You have a total of</h5>
				<h2 class="entry_count">1000000</h2>
				<h3>Entries</h3>
			</div>

			<div class="view_nav">
				<div class="view_controller active" data-view="add_entry" id="add_entry">
					Entry
				</div>
				<div class="view_controller" data-view="add_card" id="add_card">
					Card
				</div>
			</div>

			<div class="view_content">
				<div class="view_sel active" id="add_entry">
					<form action="" class="entry_form">
						<p>
							Send an entry by entering the approval code found on your purchase receipt.
						</p>
						<input class="entry_input" placeholder="Card Used for Transaction">
						<input type="number" class="entry_input" placeholder="Spend Amount Code">
						<input class="entry_input" placeholder="Approval Code">
						<a id="yo" class="btn_teal" onclick="popOpen('entry_confirm')">Upload Entry</a><!-- 
						<a class="btn_teal">Add New Card</a> -->
					</form>
				</div>
				<div class="view_sel" id="add_card">
					<form action="" class="card_form">
						<p>
							You can only add maximum of three(3) cards.
						</p>
						<input class="card_input" placeholder="Bank Issuance">
						<input class="card_input" placeholder="First Six(6) Digits">
						<input class="entry_input" placeholder="Last Four(4) Digits">
						<a class="btn_teal" onclick="popOpen('card_confirm')">Add Card</a>
					</form>
				</div>
			</div>

			<button class="btn_red">Logout</button>

			
			
			<!-- <button class="btn_red">Log-out</button> -->

			<!-- content -->
			<!-- <a href="javascript:void(0)" onclick="popOpen('custom')">Trigger popup</a> -->
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>

